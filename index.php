<?php

require __DIR__ . '/vendor/autoload.php';
use ilyajkin\Controller;
$router =
    new \DevCoder\Router([new \DevCoder\Route('api_save_review', '/api/save/review', [Controller::class, 'saveReview'], ['POST']),
    new \DevCoder\Route('api_get_id_review', '/api/get/{id}', [Controller::class, 'getByIdReview'], ['POST']),
    new \DevCoder\Route('api_get_author', '/api/get/review/author', [Controller::class, 'getByAuthor'], ['POST']),
    new \DevCoder\Route('api_update_review', '/api/update/review', [Controller::class, 'updateReview'], ['POST']),
    new \DevCoder\Route('api_delete_review', '/api/delete/{id}', [Controller::class, 'deleteReview'], ['POST']),
    new \DevCoder\Route('api_save_author', '/api/save/author', [Controller::class, 'saveAuthor'], ['POST']),
    new \DevCoder\Route('api_get_id_author', '/api/get/author/{id}', [Controller::class, 'getByIdAuthor'], ['POST']),
    new \DevCoder\Route('api_update_author', '/api/update/author', [Controller::class, 'updateAuthor'], ['POST']),
    new \DevCoder\Route('api_delete_author', '/api/delete/author/{id}', [Controller::class, 'deleteAuthor'], ['POST']),
]);

try {

    $route = $router->matchFromPath($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);

    $parameters = $route->getParameters();
    // $arguments = ['id' => 2]
    $arguments = $route->getVars();

    $controllerName = $parameters[0];
    $methodName = $parameters[1] ?? null;

    $controller = new $controllerName();
    if (!is_callable($controller)) {
        $controller =  [$controller, $methodName];
    }
    var_dump($controller(...array_values($arguments)));

} catch (\Exception $exception) {
    header("HTTP/1.0 404 Not Found");
}
