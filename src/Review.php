<?php

namespace ilyajkin;

class Review
{
    public int $id;
    public string $date;
    public int $author;
    public string $text;
}