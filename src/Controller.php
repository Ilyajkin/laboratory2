<?php

namespace ilyajkin;


use Exception;

class Controller
{
    public ReviewRepository $reviewRepository;
    public AuthorRepository $authorRepository;

    /**
     * Controller constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (!$this->checkAuthorization()) {
            throw new Exception();
        }
        $queryBuilder = new QueryBuilder();
        $reviewRepository = new ReviewRepository($queryBuilder);
        $authorRepository = new AuthorRepository($queryBuilder);
        $this->reviewRepository = $reviewRepository;
        $this->authorRepository = $authorRepository;
    }

    public function saveReview(): Review
    {
        $review = $this->reviewRepository->createFromArray($_REQUEST);
        return $this->reviewRepository->save($review);
    }

    public function getByIdReview(int $id): Review
    {
        return $this->reviewRepository->getByID($id);
    }

    public function getByAuthor(int $author): Review
    {
        return $this->reviewRepository->getByAuthor($author);
    }

    public function updateReview(array $data): Review
    {
        $review = $this->reviewRepository->createFromArray($data);
        return $this->reviewRepository->update($review);
    }

    public function deleteReview($id): void
    {
        $this->reviewRepository->delete($id);
    }

    public function createAuthor(string $nickname, string $password): Author
    {
        return $this->authorRepository->create($nickname, $password);
    }

    public function saveAuthor(array $data): Author
    {
        $author = $this->authorRepository->createFromArray($data);
        return $this->authorRepository->save($author);
    }

    public function getByIdAuthor(int $id): Author
    {
        return $this->authorRepository->getById($id);
    }

    public function updateAuthor(array $data): Author
    {
        $author = $this->authorRepository->createFromArray($data);
        return $this->authorRepository->update($author);
    }

    public function deleteAuthor(int $id): void
    {
        $this->authorRepository->delete($id);
    }

    protected function checkAuthorization(): bool
    {
        if ($_SERVER['PHP_AUTH_USER'] != 'admin' && $_SERVER['PHP_AUTH_PW'] != 'admin') {
            header('WWW-Authenticate: Basic realm="Authorization"');
            header('HTTP/1.0 401 Unauthorized');
            return false;
        }
        return true;
    }
}