<?php

namespace ilyajkin;

use ApiException;
use PDOException;

class AuthorRepository
{
    private QueryBuilder $queryBuilder;
    public const TABLENAME = 'tableAuthor';

    public function __construct(
        QueryBuilder $queryBuilder
    )
    {
        $this->queryBuilder = $queryBuilder;
        $checkTable = $this->queryBuilder->checkTable(AuthorRepository::TABLENAME);
        if ($checkTable == false) {
            $this->queryBuilder->createTable(AuthorRepository::TABLENAME);
        }

    }

    public function createFromArray(array $data): Author
    {

        $author = new Author();
        $author->id = $data['id'];
        $author->nickname = $data['nickname'];
        $author->password = $data['password'];
        return $author;
    }

    public function save(array $data)
    {
        return $this->queryBuilder->insertItem(
            array('nickname', 'password'),
            AuthorRepository::TABLENAME,
            array($data['nickname'], $data['password']),
        );
    }

    public function getById(int $id): Author
    {
        $statement = $this->queryBuilder->selectItem(
            array('*'),
            AuthorRepository::TABLENAME,
            array('id' => $id),
            1,
            0);
        return $this->createFromArray($statement);
    }

    public function update(Author $author): bool
    {
        if (empty($author->id)) {
            throw new ApiException('Такой автор не найден.');
        }
        $statement = $this->queryBuilder->updateItem(
            array($author->nickname, $author->password),
            AuthorRepository::TABLENAME,
            array('id' => $author->id),
            null,
            null,
            null
        );
        return $statement;
    }

    public function delete(int $id): bool
    {
        try {
            $statement = $this->queryBuilder->deleteItemById(
                AuthorRepository::TABLENAME,
                array('id' => $id)

            );
            return $statement;
        } catch (PDOException $e) {
            echo 'Не удалось удалить ' . $e->getMessage();
        }
    }
}