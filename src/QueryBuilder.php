<?php

namespace ilyajkin;

use ApiException;
use Exception;
use PDO;

require_once __DIR__ . '\config.php';

class QueryBuilder
{
    private PDO $pdo;
    private string $reviewTable;
    private string $authorTable;
    private string $schemaName;

    public function __construct()
    {
        global $dbName;
        global $reviewTable;
        global $authorTable;
        global $schemaName;
        $this->reviewTable = $reviewTable;
        $this->authorTable = $authorTable;
        $this->schemaName = $schemaName;
        $dsn = 'sqlite:' . __DIR__ . '/' . $dbName;
        $this->pdo = new PDO(
            $dsn,
            null,
            null,
            array(PDO::ATTR_PERSISTENT => true)
        );
    }

    public function checkTable($tableName)
    {
        $result = $this->pdo->query('SELECT name FROM sqlite_master WHERE type=\'table\' AND name=' . $tableName . ';');
        return $result;
    }

    public function createTable($tableName)
    {
        try {
            $this->pdo->query('CREATE TABLE main.' . $tableName . '(id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, author INTEGER, text TEXT);');
        } catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }
    }

    public function selectItem(array $fields, string $tableName, ?array $whereStatement, ?int $limit, ?int $offset, ?string $sort = 'ASC'): array
    {
        $query = 'SELECT ';
        if (!empty($fields)) {
            foreach ($fields as $field) {
                $query .= $field;
            }
        }

        if (!empty($tableName)) {
            $query .= ' FROM main.' . $tableName;
        } else {
            throw new Exception('Не указано имя таблицы');
        }

        if (!empty($whereStatement)) {
            foreach ($whereStatement as $key => $value) {
                $query .= ' WHERE ' . $key . ' = ' . $value;
            }
        }

        $query .= ' ORDER BY id ' . $sort;

        if (!empty($limit)) {
            $query .= ' LIMIT ' . $limit;
        }

        if (!empty($offset)) {
            $query .= ' OFFSET ' . $offset;
        }

        $query .= ';';
        return $this->pdo->query($query)->fetch();
    }

    public function insertItem(array $fields, string $tableName, array $values)
    {
        $query = 'INSERT INTO ' . $tableName;

        if (!empty($fields)) {
            $query .= '  (';
            $query .= implode(',', $fields);
            $query .= ')';
        }

        if (!empty($values)) {
            $query .= 'VALUES (';
            foreach ($values as $key => $value) {
                $value = '"' . $value . '"';
                $values[$key] = $value;
            }
            $query .= implode(',', $values);
            $query .= ');';
        }
        return $this->pdo->query($query)->execute();
    }

    public function updateItem(array $valuesUpdated, string $tableName, ?array $whereStatement, ?string $orderBy, ?int $limit, ?int $offset)
    {
        $query = 'UPDATE ' . $tableName;
        if (!empty($valuesUpdated)) {
            $query .= '  (';
            $query .= implode(',', $valuesUpdated);
            $query .= ')';

        }
        if (!empty($whereStatement)) {
            $query .= ' WHERE ';
            foreach ($whereStatement as $key => $value) {
                $value = '"' . $value . '"';
                $values[$key] = $value;
            }
            $query .= implode(',', $whereStatement);
            $query .= ');';
        }
        if (!empty($orderBy)) {
            $query .= ' ORDER BY id ' . $orderBy;
        }

        if (!empty($limit)) {
            $query .= ' LIMIT ' . $limit;
        }

        if (!empty($offset)) {
            $query .= ' OFFSET ' . $offset;
        }

        $query .= ';';
        return $this->pdo->query($query)->execute();
    }

    public function deleteItemById(string $tableName, array $whereStatement): bool
    {
        $query = 'DELETE FROM ' . $tableName;
        if (!empty($whereStatement)) {
            foreach ($whereStatement as $value) {
                $query .= ' WHERE id' . ' = ' . $value;
            }
        } else {
            throw new ApiException('Невозможно удалить. Элемент отсутствует');
        }

        $query .= ';';
        $result = $this->pdo->query($query);
        return $result;
    }
}