<?php

namespace ilyajkin;

use ApiException;
use PDOException;

class ReviewRepository
{
    private QueryBuilder $queryBuilder;
    public const TABLENAME = 'tableReview';

    public function __construct(
        QueryBuilder $queryBuilder
    )
    {
        $this->queryBuilder = $queryBuilder;
        $checkTable = $this->queryBuilder->checkTable(ReviewRepository::TABLENAME);
        if ($checkTable == false) {
            $this->queryBuilder->createTable(ReviewRepository::TABLENAME);
        }
    }

    public function createFromArray(array $data): Review
    {
        $review = new Review();
        $review->id = $data['id'];
        $review->author = $data['author'];
        $review->date = $data['date'];
        $review->text = $data['text'];
        return $review;
    }

    public function save(array $data): bool
    {
        return $this->queryBuilder->insertItem(
            array('date', 'author', 'text'),
            ReviewRepository::TABLENAME,
            array($data['date'], $data['author'], $data['text']),
        );
    }

    public function getByID(int $id): Review
    {
        $statement = $this->queryBuilder->selectItem(
            array('*'),
            ReviewRepository::TABLENAME,
            array('id' => $id),
            1,
            0);
        return $this->createFromArray($statement);
    }

    public function getByAuthor(int $author): Review
    {
        $statement = $this->queryBuilder->selectItem(
            array('*'),
            ReviewRepository::TABLENAME,
            array('author' => $author),
            1,
            0);

        return $this->createFromArray($statement);
    }

    public function update(Review $review): bool
    {
        if (empty($review->id)) {
            throw new ApiException('Отзыв не найден');
        }
        $statement = $this->queryBuilder->updateItem(
            array($review->date, $review->author, $review->text),
            ReviewRepository::TABLENAME,
            array('id' => $review->id),
            null,
            null,
            null
        );
        return $statement;
    }

    public function delete($id): bool
    {
        try {
            return $this->queryBuilder->deleteItemById(
                ReviewRepository::TABLENAME,
                array('id' => $id)
            );
        } catch (PDOException $Exception){
            throw new PDOException($Exception->getMessage( ) , $Exception->getCode( ));
        }
    }
}