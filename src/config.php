<?php

namespace ilyajkin;

global $dbName;
global $reviewTable;
global $authorTable;
global $schemaName;
$dbName = 'review.db';
$schemaName = 'storage';
$reviewTable = 'tableReview';
$authorTable = 'tableAuthor';